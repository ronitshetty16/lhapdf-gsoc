import numpy as np
import lhapdf

def test_pdf():
    pdfset = "NNPDF40_nnlo_as_0118"
    pdf = lhapdf.getPDFSet(pdfset).mkPDF()
    
    # Load expected values from file
    expected_file = "expected_values.txt"
    expected_data = np.loadtxt(expected_file)
    ids, xs, Qs, expected_vals = expected_data.T
    
    # Compute actual values using loaded PDF
    actual_vals = pdf.xfxQ(ids.astype(int), xs, Qs)
    
    # Assert that actual values are within tolerance of expected values
    rtol = 1e-6
    atol = 1e-8
    assert np.allclose(actual_vals, expected_vals, rtol=rtol, atol=atol)

if __name__ == "__main__":
    test_pdf()
